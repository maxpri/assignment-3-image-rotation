#include "../include/file.h"

#include "../include/image.h"

#include "../include/rotate.h"

#include "../include/bmp.h"


int main(int argc, char *argv[])
{

    if (argc != 3)
    {
        fprintf(stderr, "Enter input bmp file and output bmp file\n");
        return -1;
    }

    FILE *input;
    if ( !(file_open(argv[1], &input, "r") == OPEN_OK) )
    {
        fprintf(stderr, "Error while opening file\n");
        return -1;
    }

    struct image img = {0};

    if ( !((from_bmp(input, &img) == READ_OK)) )
    {
        fprintf(stderr, "Error while reading bmp\n");
        delete_image(&img);
        if ( !(file_close(input) == CLOSE_OK) ) {
            fprintf(stderr, "Error while closing file\n");
            return -1;
        }
        return -1;
    }
    if ( !(file_close(input) == CLOSE_OK) )
    {
        fprintf(stderr, "Error while closing file\n");
        delete_image(&img);
        return -1;
    }

    struct image res = rotate(img);

    delete_image(&img);
    FILE *output;

    if ( !(file_open(argv[2], &output, "w") == OPEN_OK) )
    {
        fprintf(stderr, "Error while opening output file\n");
        delete_image(&res);
        return -1;
    }

    if (!(to_bmp(output, &res) == WRITE_OK))
    {
        fprintf(stderr, "Error while writing to bmp\n");
        delete_image(&res);
        file_close(output);
        return -1;
    }

    if (file_close(output) == CLOSE_OK)
    {   delete_image(&res);
        return 0;
    }
    delete_image(&res);
    return -1;
}
