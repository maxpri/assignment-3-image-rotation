#include "../include/image.h"

struct image create_image(uint64_t width, uint64_t height)
{

    struct pixel* pixels = (struct pixel*) malloc(sizeof(struct pixel) * width * height);
    return (struct image){
        .height = height,
        .width = width,
        .data = pixels};
}

void delete_image(struct image* image)
{
    free(image->data);
}
struct pixel get_pixels(uint64_t x, uint64_t y, struct image image)
{
    return image.data[x + image.width * y];
}

void set_pixels(uint64_t x, uint64_t y, struct pixel pixels, struct image image)
{
    image.data[x + image.width * y] = pixels;
}

