#include "../include/bmp.h"

#define TYPE 19778
#define ZERO 0
#define OFFBITS 54
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24


inline static uint8_t padding_size(const uint64_t width)
{

    return (4 - (sizeof(struct pixel) * width)) % 4;
}

inline static size_t file_size(const struct image *image)
{
    return (image->width * sizeof(struct pixel) + padding_size(image->width)) * image->height;
}

inline static struct bmp_header create_header(const struct image *image)
{
    return (struct bmp_header){
        .bfType = TYPE,
        .bfileSize = file_size(image),
        .bfReserved = ZERO,
        .bOffBits = OFFBITS,
        .biSize = SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = PLANES,
        .biBitCount = BITCOUNT,
        .biCompression = ZERO,
        .biSizeImage = file_size(image) + sizeof(struct bmp_header),
        .biXPelsPerMeter = ZERO,
        .biYPelsPerMeter = ZERO,
        .biClrUsed = ZERO,
        .biClrImportant = ZERO};
}

inline static enum read_status get_header(FILE *file, struct bmp_header *header)
{
    if (!fread(header, sizeof(struct bmp_header), 1, file))
    {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *image)
{

    struct bmp_header header = {0};
    if (!(get_header(in, &header) == READ_OK))
    {
        return READ_INVALID_HEADER;
    }

    *image = create_image(header.biWidth, header.biHeight);


    for (size_t i = 0; i < image->height; ++i)
    {
        if (!fread(image->data + i * image->width, sizeof(struct pixel), image->width, in))
        {
            delete_image(image);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding_size(image->width), SEEK_CUR))
        {
            delete_image(image);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *image)
{

    struct bmp_header header = create_header(image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
    {
        return WRITE_ERROR;
    }

    if (fseek(out, header.bOffBits, SEEK_SET))
    {
        return WRITE_ERROR;
    }


    if (image->data == NULL)
    {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; ++i)
    {

        if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) != image->width || fseek(out, padding_size(image->width), SEEK_CUR) == -1)
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
