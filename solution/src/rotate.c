#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image source)
{

    struct pixel* pixels = (struct pixel*) malloc(sizeof(struct pixel) * source.width * source.height);

    struct image rotated = image_construct(source.height, source.width, pixels);

    for (size_t y = 0; y < source.height; ++y)
    {
        for (size_t x = 0; x < source.width; ++x)
        {
            set_pixels(rotated.width - 1 - y, x, get_pixels(x, y, source), rotated);
        }
    }
    return rotated;
}
