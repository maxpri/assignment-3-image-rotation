#include "../include/file.h"

#include <stdio.h>

enum open_status file_open(char* fileName, FILE** file, char* mode) {

    *file = fopen(fileName, mode);

    if (file == NULL) {
        return OPEN_ERROR;
    }

    return OPEN_OK;

}

enum close_status file_close(FILE* file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
