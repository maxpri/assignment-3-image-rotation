#ifndef FILE_H
#define FILE_H

#include "image.h"

#include <stdbool.h>
#include <stdio.h>

enum open_status {
    OPEN_OK,
    OPEN_ERROR
};

enum open_status file_open(char *fileName, FILE **file, char *mode);

enum close_status {
    CLOSE_OK,
    CLOSE_ERROR
};

enum close_status file_close(FILE *file);

#endif
