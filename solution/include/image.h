#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height);

void delete_image(struct image* image);

static inline struct image image_construct(uint64_t width, uint64_t height, struct pixel *data)
{
    return (struct image){.width = width, .height = height, .data = data};
}

struct pixel get_pixels(uint64_t x, uint64_t y, struct image image);

void set_pixels(uint64_t x, uint64_t y, struct pixel pixels, struct image image);

#endif
